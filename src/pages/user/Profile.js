import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import Header from '../../components/header/Header';
import ProfileInfo from '../../components/users/ProfileInfo';
import GameHistory from '../../components/users/GameHistory';

function Profile() {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = localStorage.getItem('id');

    if (token && id) {
      axios.get(`https://viridian-indri-tutu.cyclic.cloud/users/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => {
          setUserData(response.data.data);
        })
        .catch((error) => {
          console.error('Error fetching user data:', error);
        });
    }
  }, []);

  return (
    <div>
      <Header />
      <Container fluid className='banner-section'>
        <Container className='banner bg-cover game-container'>
          <Row className="justify-content-center">
            <Col lg={4} md={4} sm={12} className='transparent-bg'>
              {userData && <ProfileInfo userData={userData} />}
            </Col>
            <Col lg={8} md={10} sm={12} className='transparent-bg table-responsive'>
              {userData && <GameHistory userData={userData} />}
            </Col>
          </Row>
        </Container>
      </Container>
    </div>
  );
}

export default Profile;
