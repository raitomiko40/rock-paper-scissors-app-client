import React, { useState,useEffect } from 'react';
import axios from 'axios';
import Loading from '../../loading'
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col, Form, Button, InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandBackFist, faHand, faHandScissors } from '@fortawesome/free-solid-svg-icons';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true); // Set loading to true initially
  const navigate = useNavigate();

  useEffect(() => {
    const checkLoginStatus = async () => {
      const token = localStorage.getItem('token');
      if (token) {
        // Jika pengguna sudah login, arahkan ke halaman permainan
        navigate('/game');
      } else {
        // If not logged in, set loading to false
        setLoading(false);
      }
    };

    checkLoginStatus();
  }, [navigate]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setLoading(true); // Set loading state to true when form is submitted
      const response = await axios.post('https://viridian-indri-tutu.cyclic.cloud/auth/login', {
        username: username,
        password: password,
      });

      const { accesstoken, uid } = response.data;

      // Simpan token dan ID pengguna dalam penyimpanan lokal
      localStorage.setItem('token', accesstoken);
      localStorage.setItem('id', uid);

      navigate('/game');
    } catch (error) {
      setError(error.response ? error.response.data.error : 'Terjadi kesalahan yang tidak terduga.');
    } finally {
      setLoading(false); // Set loading state to false after request completes (success or error)
    }
  };

  return (
    <Container fluid className="login-page">
      { 
        loading ?
        (
          <Loading />
        )
        :
        (

      <Row className="justify-content-center align-items-center min-vh-100">
        <Col lg={6} className="offset-lg-1">
          <div className="bg-white shadow rounded">
            <Row>
              <Col md={7} className="">
                <div className="form-left h-100 py-5 px-5">
                <h3 className="mb-3 title">Login Now</h3>
                  <Form onSubmit={handleSubmit}>
                  <p className='has-text-center text-danger'>{error}</p>
                    <Form.Group controlId="username" className='mb-3'>
                      <Form.Label>Username<span className="text-danger">*</span></Form.Label>
                      <InputGroup>
                        <InputGroup.Text><i className="bi bi-person-fill"></i></InputGroup.Text>
                        <Form.Control value={username} onChange={(e) => setUsername(e.target.value)} type="text" placeholder="Enter Username" className="col" />
                      </InputGroup>
                    </Form.Group>

                    <Form.Group controlId="password" className='mb-1'>
                      <Form.Label>Password<span className="text-danger">*</span></Form.Label>
                      <InputGroup>
                        <InputGroup.Text><i className="bi bi-lock-fill"></i></InputGroup.Text>
                        <Form.Control value={password} onChange={(e) => setPassword(e.target.value)} type="password" placeholder="Enter Password" className="col" />
                      </InputGroup>
                    </Form.Group>

                    <Row className="mb-3 mt-4">
                      <Col sm={6}>
                        <Button type="submit" variant="primary" className="px-4">Login</Button>
                      </Col>
                      <Col sm={6} className="text-end">
                        <small className="text-muted">Belum punya akun? <a href="/register" className="text-primary">Daftar sekarang!</a></small>
                      </Col>
                    </Row>

                  </Form>
                </div>
              </Col>
              <Col md={5} className="ps-0 d-none d-md-block">
                <div className="form-right h-100 bg-login text-white text-center pt-5">
                  <div className='mb-3 mt-5 h1'>
                    <FontAwesomeIcon className='icon' icon={faHandBackFist}/>
                    <FontAwesomeIcon className='icon' icon={faHand}/>
                    <FontAwesomeIcon className='icon' icon={faHandScissors}/>
                  </div>
                  <h3 className="h6">Silahkan login sebelum memulai permainan</h3>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
        )

      }
    </Container>
  );
};

export default Login;
