import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col, Form, Button, InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Loading from '../../loading';
import { faHandBackFist, faHand, faHandScissors } from '@fortawesome/free-solid-svg-icons';

const Register = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false); // Set loading to false initially
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setLoading(true); // Set loading state to true when form is submitted

      // Validasi data
      if (!username || !password) {
        setError('Semua field harus diisi');
        return;
      }

      if (username.length < 5) {
        setError('Nama pengguna harus memiliki minimal 5 karakter');
        return;
      }

      if (password.length < 6) {
        setError('Kata sandi harus memiliki minimal 6 karakter');
        return;
      }

      const response = await axios.post('https://viridian-indri-tutu.cyclic.cloud/auth/register', {
        username: username,
        password: password,
      });

      if (response.status === 201) {
        // Pendaftaran berhasil
        navigate('/');
      } else {
        setError('Terjadi kesalahan saat mendaftar');
      }
    } catch (error) {
      setError(error.response ? error.response.data.error : 'Terjadi kesalahan yang tidak terduga.');
    } finally {
      setLoading(false); // Set loading state to false after request completes (success or error)
    }
  };

  return (
    <Container fluid className="register-page">
      {loading ? 
      (<Loading />)
       :
       (
      <Row className="justify-content-center align-items-center min-vh-100">
        <Col lg={6} className="offset-lg-1">
          <div className="bg-white shadow rounded">
            <Row>
              <Col md={7} className="">
                <div className="form-left h-100 py-5 px-5">
                  <h3 className="mb-3 title">Register Now</h3>
                  <Form onSubmit={handleSubmit}>
                    <p className='has-text-center text-danger'>{error}</p>
                    <Form.Group controlId="username" className='mb-3'>
                      <Form.Label>Username<span className="text-danger">*</span></Form.Label>
                      <InputGroup>
                        <InputGroup.Text><i className="bi bi-person-fill"></i></InputGroup.Text>
                        <Form.Control value={username} onChange={(e) => setUsername(e.target.value)} type="text" placeholder="Enter Username" className="col" />
                      </InputGroup>
                    </Form.Group>

                    <Form.Group controlId="password" className='mb-1'>
                      <Form.Label>Password<span className="text-danger">*</span></Form.Label>
                      <InputGroup>
                        <InputGroup.Text><i className="bi bi-lock-fill"></i></InputGroup.Text>
                        <Form.Control value={password} onChange={(e) => setPassword(e.target.value)} type="password" placeholder="Enter Password" className="col" />
                      </InputGroup>
                    </Form.Group>

                    <Row className="mb-3 mt-4">
                      <Col sm={6}>
                        <Button type="submit" variant="primary" className="px-4">
                          Register
                        </Button>
                      </Col>
                      <Col sm={6} className="text-end">
                        <small className="text-muted">Sudah punya akun? <a href="/" className="text-primary">Silahkan Login!</a></small>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </Col>
              <Col md={5} className="ps-0 d-none d-md-block">
                <div className="form-right h-100 bg-register text-white text-center pt-5">
                  <div className='mb-3 mt-5 h1'>
                    <FontAwesomeIcon className='icon' icon={faHandBackFist}/>
                    <FontAwesomeIcon className='icon' icon={faHand}/>
                    <FontAwesomeIcon className='icon' icon={faHandScissors}/>
                  </div>
                  <h3 className="h6">Silahkan daftar sebelum memulai permainan</h3>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>

       ) }
    </Container>
  );
};

export default Register;
