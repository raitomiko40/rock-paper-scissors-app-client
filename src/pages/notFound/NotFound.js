import React from 'react';

const NotFound = () => {
  return (
    <div className="d-flex justify-content-center align-items-center min-vh-100">
      <div className="text-center">
        <h1>404 - Not Found</h1>
        <p>Halaman yang Anda cari tidak ditemukan.</p>
      </div>
    </div>
  );
}

export default NotFound;
