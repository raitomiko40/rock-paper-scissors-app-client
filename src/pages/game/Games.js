import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Button, Container, Row, Col, Alert } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandBackFist, faHand, faHandScissors, faRotate } from '@fortawesome/free-solid-svg-icons';
import Header from '../../components/header/Header';


function Games() {
  const token = localStorage.getItem('token');
  const id = localStorage.getItem('id');
  const [playerChoice, setPlayerChoice] = useState(null);
  const [botChoice, setBotChoice] = useState(null);
  const [gameResult, setGameResult] = useState(null);
  const [botChoiceButtonClass, setBotChoiceButtonClass] = useState({
    rock: '',
    paper: '',
    scissors: '',
  });
  const [isGameOver, setIsGameOver] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const handleChoice = async (choice) => {
    // Jika pemain sudah memilih atau permainan selesai, tampilkan pesan peringatan
    if (playerChoice || isGameOver) {
      setShowAlert(true);
      return;
    }
    if (!id || !token) {
      console.error('Token atau ID pengguna tidak tersedia');
      return;
    }

    try {
      const response = await axios.post(`https://viridian-indri-tutu.cyclic.cloud/game/start/${id}`, { playerChoice: choice }, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        const data = response.data.data;
        setPlayerChoice(data.playerChoice);
        setBotChoice(data.botChoice);
        setGameResult(data.result);
        setIsGameOver(true);
      }
    } catch (error) {
      console.error(error);
      // Handle error here
    }
  };

  useEffect(() => {
    // Ketika botChoice berubah, atur kelas CSS tambahan pada tombol Auto Bot Move
    if (botChoice) {
      const updatedBotChoiceButtonClass = {
        rock: '',
        paper: '',
        scissors: '',
      };
      updatedBotChoiceButtonClass[botChoice] = 'active-choice';
      setBotChoiceButtonClass(updatedBotChoiceButtonClass);
    }
  }, [botChoice]);

  const handleRestart = () => {
    setPlayerChoice(null);
    setBotChoice(null);
    setGameResult(null);
    setIsGameOver(false);
    setBotChoiceButtonClass({
      rock: '',
      paper: '',
      scissors: '',
    });
    setShowAlert(false); // Sembunyikan pesan peringatan saat mulai lagi
  };

  return (
    <div>
      <Header />
      <Container fluid className='banner-section '>
        <Container className='banner bg-cover game-container '>
          <Row className="justify-content-center ">
            <Col md={12} sm={1} className='transparent-bg'>
              <h3 className='text-center mb-5'>Selamat Bermain</h3>
              <Row className="text-center">
                <Col md={2} className='mb-4 d-flex flex-column gap-4'>
                  <h4 className='mb-3'>Choose Your Move:</h4>
                  
                  <Button
                    variant="primary"
                    onClick={() => handleChoice('rock')}
                    className={playerChoice === 'rock' ? 'active-choice' : ''}
                  >
                    <FontAwesomeIcon icon={faHandBackFist} />
                    Rock
                  </Button>
                  <Button
                    variant="primary"
                    onClick={() => handleChoice('paper')}
                    className={playerChoice === 'paper' ? 'active-choice' : ''}
                  >
                    <FontAwesomeIcon icon={faHand} />
                    Paper
                  </Button>
                  <Button
                    variant="primary"
                    onClick={() => handleChoice('scissors')}
                    className={playerChoice === 'scissors' ? 'active-choice' : ''}
                  >
                    <FontAwesomeIcon icon={faHandScissors} />
                    Scissors
                  </Button>
                </Col>
                <Col className='m-4 d-flex align-items-center justify-content-center'>
                  {playerChoice && (
                    <h4 className="vs-text text-success"> {gameResult}</h4>
                  )}
                  {!playerChoice && <h4 className="vs-text">VS</h4>}
                </Col>
                <Col md={2} className='mb-4 d-flex flex-column gap-4'>
                  <h4 className='mb-3'>Auto Bot Move:</h4>
                  <Button
                    variant="primary"
                    className={botChoiceButtonClass.rock}
                  >
                    <FontAwesomeIcon icon={faHandBackFist} />
                    Rock
                  </Button>
                  <Button
                    variant="primary"
                    className={botChoiceButtonClass.paper}
                  >
                    <FontAwesomeIcon icon={faHand} />
                    Paper
                  </Button>
                  <Button
                    variant="primary"
                    className={botChoiceButtonClass.scissors}
                  >
                    <FontAwesomeIcon icon={faHandScissors} />
                    Scissors
                  </Button>
                </Col>
              </Row>
              <Row className='text-center'>
                <Col className='mb-4'>
                  <Button onClick={handleRestart}>
                    <FontAwesomeIcon icon={faRotate} />
                    Mulai Lagi
                  </Button>
                </Col>
              </Row>
              {showAlert && (
                <Row className='text-center'>
                  <Col>
                    <Alert variant='danger'>Silakan klik "Mulai Lagi" untuk memilih lagi.</Alert>
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Container>
      </Container>
    </div>
  );
}

export default Games;
