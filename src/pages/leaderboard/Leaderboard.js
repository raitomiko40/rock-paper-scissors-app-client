import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import axios from 'axios';
import Header from '../../components/header/Header';

function Leaderboard() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Panggil API untuk mendapatkan data pengguna
    axios.get('https://viridian-indri-tutu.cyclic.cloud/users')
      .then((response) => {
        const data = response.data.data;
        // Mengurutkan data berdasarkan total score tertinggi
        const sortedUsers = data.sort((a, b) => b.totalScore - a.totalScore);

        // Menambahkan peringkat (rank) pada setiap pengguna
        sortedUsers.forEach((user, index) => {
          user.rank = index + 1;
        });

        setUsers(sortedUsers);
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
      });
  }, []);

  return (
    <div>
      <Header />
      <Container fluid className='banner-section'>
        <Container className='banner bg-cover game-container'>
          <Row className="justify-content-center">
            <Col md={12} className='transparent-bg'>
              <h3 className='text-center mb-5'>Leaderboard</h3>
              <Row className="text-center">
                <Col md={12}>
                  <Table striped bordered hover>
                    <thead>
                      <tr>
                        <th>Rank</th>
                        <th>Username</th>
                        <th>Total Score</th>
                      </tr>
                    </thead>
                    <tbody>
                      {users.map((user) => (
                        <tr key={user.id}>
                          <td>{user.rank}</td>
                          <td>{user.username}</td>
                          <td>{user.totalScore !== null ? user.totalScore : 0}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </Container>
    </div>
  );
}

export default Leaderboard;
