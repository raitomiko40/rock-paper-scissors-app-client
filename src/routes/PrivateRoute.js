import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const PrivateRoute = ({ children }) => {
  const navigate = useNavigate();
  const token = localStorage.getItem('token');

  useEffect(() => {
    if (!token) {
      // Jika tidak ada token, arahkan pengguna kembali ke halaman login
      navigate('/');
    } 
  }, [navigate, token]);

  // Hanya merender children (komponen yang ada dalam PrivateRoute) jika token tersedia
  return token ? children : null;
};

export default PrivateRoute;
