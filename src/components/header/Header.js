import React, { useState } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Loading from '../../loading';

function Header() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [expanded, setExpanded] = useState(false);

  const handleLogout = () => {
    setLoading(true); // Set loading state to true during logout

    // Simulate some async action (e.g., API call, logout process)
    setTimeout(() => {
      // Hapus token dan ID pengguna dari penyimpanan lokal
      localStorage.removeItem('token');
      localStorage.removeItem('id');

      // Hapus cookie dengan nama yang sesuai
      document.cookie = 'nama_cookie=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';

      navigate('/'); // Gantilah dengan path halaman login yang sesuai

      setLoading(false); // Set loading state to false after logout
    }, 1000); // Adjust the timeout as needed
  };

  return (
    <Navbar className='nav-container custom-navbar ' expand="md" expanded={expanded}>
      {loading ? (
        <Loading />
      ) : (
        <>
          <Navbar.Brand className='container' href="/">
            Rock Paper Scissors App
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => setExpanded(!expanded)} />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="/game">Games</Nav.Link>
              <Nav.Link href="/leaderboard">Leaderboard</Nav.Link>
              <Nav.Link href="/profile">Profile</Nav.Link>
              <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </>
      )}
    </Navbar>
  );
}

export default Header;
