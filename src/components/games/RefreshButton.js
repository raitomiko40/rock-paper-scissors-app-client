import React from 'react';
import refreshGame from '../../assets/img/refresh.png'

const RefreshButton = ({ resetGame }) => {
  return (
    <div className="text-center option">
      <img
        id="refresh"
        className="refresh"
        role="button"
        src={refreshGame}
        alt="refresh"
        onClick={resetGame}
      />
    </div>
  );
}

export default RefreshButton;
