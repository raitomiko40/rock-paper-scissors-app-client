import React, { useEffect } from 'react';

const VSResult = (props) => {
  const { result } = props;

  useEffect(() => {
    console.log('Hasil dalam komponen Game:', result);
  }, [result]);

  return (
    <div className="col d-flex justify-content-center align-items-center">
      {result !== null ? (
        <div className={`d-flex justify-content-center align-items-center results ${result}`}>
          {result === 'Player Win' && (
            <div>Player<br />MENANG</div>
          )}
          {result === 'Bot Win' && (
            <div>Bot<br />MENANG</div>
          )}
          {result === 'Draw' && (
            <div>SERI</div>
          )}
        </div>
      ) : (
        <div id="vs" className="vs">VS</div>
      )}
    </div>
  );
}

export default VSResult;
