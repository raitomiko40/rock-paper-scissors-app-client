import React, { useState, useEffect } from 'react';
import axios from 'axios';
import batuImg from '../../assets/img/batu1.png';
import guntingImg from '../../assets/img/gunting1.png';
import kertasImg from '../../assets/img/kertas1.png';

const Player = (props) => {
  const [username, setUsername] = useState('');
  const token = localStorage.getItem('token');
  const id = localStorage.getItem('id');

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (!token || !id) {
          console.error('Token atau ID pengguna tidak tersedia');
          return;
        }

        const response = await axios.get(`https://viridian-indri-tutu.cyclic.cloud/users/${id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        const { username } = response.data.data;
        setUsername(username);
      } catch (error) {
        console.error('Terjadi kesalahan:', error);
        // Tampilkan pesan kesalahan kepada pengguna
      }
    };

    fetchData();
  }, [id, token]);

  return (
    <div className="col" id="Player">
      <div className="mt-5 mb-5">
        <h1>{username}</h1>
      </div>
      <div className="mb-3 option">
        <img
          id="batu"
          className={`player ${props.playerChoice === 'batu' ? 'activePick' : ''}`}
          role="button"
          src={batuImg}
          alt="batu"
          onClick={() => props.handlePlayerChoice('batu')}
        />
      </div>
      <div className="mb-3 option">
        <img
          id="gunting"
          className={`player ${props.playerChoice === 'gunting' ? 'activePick' : ''}`}
          role="button"
          src={guntingImg}
          alt="gunting"
          onClick={() => props.handlePlayerChoice('gunting')}
        />
      </div>
      <div className="mb-3 option">
        <img
          id="kertas"
          className={`player ${props.playerChoice === 'kertas' ? 'activePick' : ''}`}
          role="button"
          src={kertasImg}
          alt="kertas"
          onClick={() => props.handlePlayerChoice('kertas')}
        />
      </div>
    </div>
  );
}

export default Player;
