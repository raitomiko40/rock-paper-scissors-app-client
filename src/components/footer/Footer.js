import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Footer = () => {
  return (
    <div className="container-fluid bg-white footer">
      <Container className="py-5">
        <Row className="g-5">
          <Col md={6} lg={3} className="wow fadeIn" data-wow-delay="0.1s">
            <a href="index.html" className="d-inline-block mb-3">
              <h1 className="text-primary">Hairnic</h1>
            </a>
            <p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis aliquet, erat non malesuada consequat, nibh erat tempus risus, vitae porttitor purus nisl vitae purus.</p>
          </Col>
          <Col md={6} lg={3} className="wow fadeIn" data-wow-delay="0.3s">
            <h5 className="mb-4">Get In Touch</h5>
            <p><i className="fa fa-map-marker-alt me-3"></i>123 Street, New York, USA</p>
            <p><i className="fa fa-phone-alt me-3"></i>+012 345 67890</p>
            <p><i className="fa fa-envelope me-3"></i>info@example.com</p>
            <div className="d-flex pt-2">
              <a className="btn btn-square btn-outline-primary me-1" href=""><i className="fab fa-twitter"></i></a>
              <a className="btn btn-square btn-outline-primary me-1" href=""><i className="fab fa-facebook-f"></i></a>
              <a className="btn btn-square btn-outline-primary me-1" href=""><i className="fab fa-instagram"></i></a>
              <a className="btn btn-square btn-outline-primary me-1" href=""><i className="fab fa-linkedin-in"></i></a>
            </div>
          </Col>
          <Col md={6} lg={3} className="wow fadeIn" data-wow-delay="0.5s">
            <h5 className="mb-4">Our Products</h5>
            <a className="btn btn-link" href="">Hair Shining Shampoo</a>
            <a className="btn btn-link" href="">Anti-dandruff Shampoo</a>
            <a className="btn btn-link" href="">Anti Hair Fall Shampoo</a>
            <a className="btn btn-link" href="">Hair Growing Shampoo</a>
            <a className="btn btn-link" href="">Anti smell Shampoo</a>
          </Col>
          <Col md={6} lg={3} className="wow fadeIn" data-wow-delay="0.7s">
            <h5 className="mb-4">Popular Link</h5>
            <a className="btn btn-link" href="">About Us</a>
            <a className="btn btn-link" href="">Contact Us</a>
            <a className="btn btn-link" href="">Privacy Policy</a>
            <a className="btn btn-link" href="">Terms & Condition</a>
            <a className="btn btn-link" href="">Career</a>
          </Col>
        </Row>
      </Container>
      <Container className="wow fadeIn" data-wow-delay="0.1s">
        <div className="copyright">
          <Row>
            <Col md={6} className="text-center text-md-start mb-3 mb-md-0">
              &copy; <a className="border-bottom" href="#">Your Site Name</a>, All Right Reserved.
              Designed By <a className="border-bottom" href="https://htmlcodex.com">HTML Codex</a> Distributed by <a href="https://themewagon.com">ThemeWagon</a>
            </Col>
            <Col md={6} className="text-center text-md-end">
              <div className="footer-menu">
                <a href="">Home</a>
                <a href="">Cookies</a>
                <a href="">Help</a>
                <a href="">FAQs</a>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default Footer;
