import React from 'react';
import { Card } from 'react-bootstrap';
import defaultImage from '../../assets/img/default-profile.png';

const ProfileInfo = ({ userData }) => {
  const imageUrl = userData.imgUrl || defaultImage;

  return (
    <Card className="sidebar-card">
      <Card.Body>
        <div className="sidebar-content">
          <img
            src={imageUrl}
            alt="Profile"
            className='profile-image'
          />
          <Card.Title>{userData.username}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">ID: {userData.id}</Card.Subtitle>
          <Card.Text>Email: {userData.email}</Card.Text>
          <Card.Text>Total Score: {userData.totalScore || 0}</Card.Text>
        </div>
      </Card.Body>
    </Card>
  );
};

export default ProfileInfo;
