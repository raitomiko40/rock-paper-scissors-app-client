import React, { useState, useEffect } from 'react';
import { orderBy } from 'lodash';
import { Table, Button } from 'react-bootstrap';

const GameHistory = ({ userData }) => {
  const [historyPage, setHistoryPage] = useState(0);
  const itemsPerPage = 10;

  useEffect(() => {
    if (userData) {
      setHistoryPage(0);
    }
  }, [userData]);

  if (!userData) {
    return <p>Loading game history...</p>;
  }

  const totalHistoryPages = Math.ceil(userData.game.length / itemsPerPage);

  const handleNextPage = () => {
    if (historyPage < totalHistoryPages - 1) {
      setHistoryPage(historyPage + 1);
    }
  };

  const handlePrevPage = () => {
    if (historyPage > 0) {
      setHistoryPage(historyPage - 1);
    }
  };

  return (
    <div>
      <h4 className='mb-4'>History Permainan</h4>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>No</th>
            <th>Score</th>
            <th>Player Choice</th>
            <th>Bot Choice</th>
            <th>Result</th>
            <th>Tanggal Bermaint</th>
          </tr>
        </thead>
        <tbody>
          {orderBy(
            userData.game,
            ['createdAt'], // Urutkan berdasarkan createdAt
            ['desc'] // Urutan descending (terbaru ke yang lama)
          )
            .slice(historyPage * itemsPerPage, (historyPage + 1) * itemsPerPage)
            .map((game, index) => (
              <tr key={game.id}>
                <td>{index + 1}</td>
                <td>{game.score}</td>
                <td>{game.playerChoice}</td>
                <td>{game.botChoice}</td>
                <td>{game.result}</td>
                <td>{new Date(game.createdAt).toLocaleString()}</td>
              </tr>
            ))}
        </tbody>
      </Table>
      <div className="text-center">
        <Button
          variant="primary"
          onClick={handlePrevPage}
          disabled={historyPage === 0}
        >
          Previous
        </Button>{' '}
        <Button
          variant="primary"
          onClick={handleNextPage}
          disabled={historyPage === totalHistoryPages - 1}
        >
          Next
        </Button>
      </div>
    </div>
  );
};

export default GameHistory;
