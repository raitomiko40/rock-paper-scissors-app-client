
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Menggunakan Route dan Routes
import Games from './pages/game/Games';
import Leaderboard from './pages/leaderboard/Leaderboard';
import Profile from './pages/user/Profile';
import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import NotFound from './pages/notFound/NotFound';
import PrivateRoute from './routes/PrivateRoute';
import './App.css';

const App = () => {


  return (
    <Router>
      <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/profile" element={<PrivateRoute><Profile /></PrivateRoute>} />
            <Route path="/leaderboard" element={<PrivateRoute><Leaderboard /></PrivateRoute>} />
            <Route path="/game" element={<PrivateRoute><Games /></PrivateRoute>} />
            <Route path="*" element={<NotFound />} /> 
      </Routes>
    </Router>
  );
};

export default App;
