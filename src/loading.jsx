const Loading = () => {
    return (
      <div className="d-flex justify-content-center align-items-center text-4xl min-vh-100">
        <div className="loading"></div>
      </div>
    );
  };
  
  export default Loading;
  